from unknown_source import *


def test_getoddnumbers_zero():
	assert get_odd_numbers(0) == []


def test_getoddnumbers_one():
	assert get_odd_numbers(1) == [1]


def test_getoddnumbers_twelve():
	assert get_odd_numbers(12) == [1, 3, 5, 7, 9, 11]


def test_iselementinlist_false():
	assert is_element_in_list('4', ['1', '2', '3']) is False


def test_iselementinlist_true():
	assert is_element_in_list('2', ['1', '2', '3']) is True


def test_reverselist():
	assert reverse_list([1, 2, 4, 8, 16]) == [16, 8, 4, 2, 1]

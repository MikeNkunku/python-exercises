from pytest import fail

from w3resource.puzzles import *


def test_are_exactly_four_distinct_values_present_without_consecutive_duplicates_for_first_twenty_entries_use_case_1_true():
    numbers = [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4]
    assert \
        are_exactly_four_distinct_values_present_without_consecutive_duplicates_for_first_twenty_entries(numbers)


def test_are_exactly_four_distinct_values_present_without_consecutive_duplicates_for_first_twenty_entries_use_case_2_false():
    numbers = [1, 2, 3, 3, 1, 2, 3, 3, 1, 2, 3, 3, 1, 2, 3, 3]
    assert \
        not are_exactly_four_distinct_values_present_without_consecutive_duplicates_for_first_twenty_entries(numbers)


def test_are_exactly_four_distinct_values_present_without_consecutive_duplicates_for_first_twenty_entries_use_case_3_false():
    numbers = [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]
    assert \
        not are_exactly_four_distinct_values_present_without_consecutive_duplicates_for_first_twenty_entries(numbers)


def test_get_indexes_of_numbers_below_provided_threshold_use_case_1():
    numbers = [0, 12, 45, 3, 4923, 322, 105, 29, 15, 39, 55]
    expected_indexes = [0, 1, 2, 3, 7, 8, 9, 10]

    assert get_indexes_of_numbers_below_provided_threshold(100, numbers)\
           == expected_indexes


def test_get_indexes_of_numbers_below_provided_threshold_use_case_2():
    numbers = [0, 12, 4, 3, 49, 9, 1, 5, 3]
    expected_indexes = [0, 2, 3, 5, 6, 7, 8]

    assert get_indexes_of_numbers_below_provided_threshold(10, numbers)\
           == expected_indexes


def internal_test_get_indexes_of_numbers_which_sum_to_zero(
        numbers: list[int], expected_indexes: tuple[int, int]
):
    indexes = get_indexes_of_numbers_which_sum_to_zero(numbers)

    assert isinstance(indexes, tuple)
    assert len(indexes) == len(expected_indexes)
    for expected_index in expected_indexes:
        assert expected_index in indexes


def test_get_indexes_of_numbers_which_sum_to_zero_use_case_1():
    numbers = [1, -4, 6, 7, 4]
    expected_indexes = (4, 1)

    internal_test_get_indexes_of_numbers_which_sum_to_zero(
        numbers, expected_indexes)


def test_get_indexes_of_numbers_which_sum_to_zero_use_case_2():
    numbers = [1232, -20352, 12547, 12440, 741, 341, 525, 20352, 91, 20]
    expected_indexes = (1, 7)

    internal_test_get_indexes_of_numbers_which_sum_to_zero(
        numbers, expected_indexes)


def test_get_indexes_of_numbers_which_sum_to_zero_use_case_3():
    numbers = [1232, -20352, 12547, 12440, 741, 341, 525, 91, 20]

    assert get_indexes_of_numbers_which_sum_to_zero(numbers) is None


def internal_test_get_indices_for_which_numbers_in_list_drop(
        numbers: list[int], expected_indices: list[int]):
    indices = get_indices_for_which_numbers_in_list_drop(numbers)

    assert len(indices) == len(expected_indices)
    for expected_index in expected_indices:
        assert expected_index in indices


def test_get_indices_for_which_numbers_in_list_drop_use_case_1():
    numbers = [0, -1, 3, 8, 5, 9, 8, 14, 2, 4, 3, -10, 10, 17, 41, 22, -4, -4,
               -15, 0]
    expected_indices = [1, 4, 6, 8, 10, 11, 15, 16, 18]
    internal_test_get_indices_for_which_numbers_in_list_drop(
        numbers, expected_indices)


def test_get_indices_for_which_numbers_in_list_drop_use_case_2():
    numbers = [6, 5, 4, 3, 2, 1]
    expected_indices = [1, 2, 3, 4, 5]
    internal_test_get_indices_for_which_numbers_in_list_drop(
        numbers, expected_indices)


def test_get_indices_for_which_numbers_in_list_drop_use_case_3():
    numbers = [1, 19, 5, 15, 5, 25, 5]
    expected_indices = [2, 4, 6]
    internal_test_get_indices_for_which_numbers_in_list_drop(
        numbers, expected_indices)


def test_get_list_of_strings_with_fewer_total_characters_use_case_1():
    lists = [['this', 'list', 'is', 'narrow'], ['I', 'am', 'shorter but wider']]
    expected_list = ['this', 'list', 'is', 'narrow']

    assert get_list_of_strings_with_fewer_total_characters(lists)\
           == expected_list


def test_get_list_of_strings_with_fewer_total_characters_use_case_2():
    lists = [['Red', 'Black', 'Pink'], ['Green', 'Red', 'White']]
    expected_list = ['Red', 'Black', 'Pink']

    assert get_list_of_strings_with_fewer_total_characters(lists)\
           == expected_list


def test_get_list_of_strings_with_fewer_total_characters_without_lists():
    try:
        get_list_of_strings_with_fewer_total_characters([])
        fail('An exception should have been thrown by then')
    except ValueError as error:
        assert str(error) == 'At least one list should be present'


def test_get_list_sorted_by_digits_sum_use_case_1():
    numbers = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    expectation = [10, 11, 20, 12, 13, 14, 15, 16, 17, 18, 19]

    assert get_list_sorted_by_digits_sum(numbers) == expectation


def test_get_list_sorted_by_digits_sum_use_case_2():
    numbers = [23, 2, 9, 34, 8, 9, 10, 74]
    expectation = [10, 2, 23, 34, 8, 9, 9, 74]

    assert get_list_sorted_by_digits_sum(numbers) == expectation


def internal_test_get_positions_of_uppercase_vowels_at_even_indices_in_string(
        string: str, expected_indices: list[int]):
    indices = get_positions_of_uppercase_vowels_at_even_indices_in_string(
        string)

    assert len(indices) == len(expected_indices)
    for expected_index in expected_indices:
        assert expected_index in indices


def test_get_positions_of_uppercase_vowels_at_even_indices_in_string_use_case_1():
    internal_test_get_positions_of_uppercase_vowels_at_even_indices_in_string(
        'w3rEsOUrcE', [6])


def test_get_positions_of_uppercase_vowels_at_even_indices_in_string_use_case_2():
    internal_test_get_positions_of_uppercase_vowels_at_even_indices_in_string(
        'AEIOUYW', [0, 2, 4])


def test_get_product_of_odd_digits_in_number_with_odd_digits_1():
    assert get_product_of_odd_digits_in_number(123456789) == 945


def test_get_product_of_odd_digits_in_number_with_odd_digits_2():
    assert get_product_of_odd_digits_in_number(13579) == 945


def test_get_product_of_odd_digits_in_number_without_odd_digits():
    assert get_product_of_odd_digits_in_number(2468) == 0


def test_get_string_split_by_spaces_or_commas_with_commas():
    assert get_string_split_by_spaces_or_commas('a,b,c,d')\
           == ['a', 'b', 'c', 'd']


def test_get_string_split_by_spaces_or_commas_with_spaces():
    assert get_string_split_by_spaces_or_commas('a b c d')\
           == ['a', 'b', 'c', 'd']


def test_get_string_split_by_spaces_or_commas_without_separator():
    assert get_string_split_by_spaces_or_commas('abcd')\
           == ['b', 'd']


def test_get_strings_containing_substring_with_ca():
    strings = ('cat', 'car', 'fear', 'center')
    expected_strings = ['cat', 'car']

    assert get_strings_containing_substring('ca', strings) == expected_strings


def test_get_strings_containing_substring_with_o():
    strings = ('cat', 'dog', 'shatter', 'donut', 'at', 'todo', '')
    expected_strings = ['dog', 'donut', 'todo']

    assert get_strings_containing_substring('o', strings) == expected_strings


def test_get_strings_containing_substring_with_oe():
    strings = ('cat', 'dog', 'shatter', 'donut', 'at', 'todo', '')

    assert get_strings_containing_substring('oe', strings) == []


def test_get_strings_lengths_use_case_1():
    strings = ['cat', 'car', 'fear', 'center']
    expected_lengths = [3, 3, 4, 6]

    assert get_strings_lengths(strings) == expected_lengths


def test_get_strings_lengths_use_case_2():
    strings = ['cat', 'dog', 'shatter', 'donut', 'at', 'todo', '']
    expected_lengths = [3, 3, 7, 5, 2, 4, 0]

    assert get_strings_lengths(strings) == expected_lengths


def test_get_sum_of_ascii_values_for_uppercase_letters_in_string_python_exercises_373():
    assert get_sum_of_ascii_values_for_uppercase_letters_in_string(
        'PytHon ExerciSEs') == 373


def test_get_sum_of_ascii_values_for_uppercase_letters_in_string_javascript_157():
    assert get_sum_of_ascii_values_for_uppercase_letters_in_string(
        'JavaScript') == 157


def test_get_sum_for_numbers_with_more_than_2_digits_among_k_first_values_use_case_1():
    numbers = [4, 5, 17, 9, 14, 108, -9, 12, 76]

    assert get_sum_for_numbers_with_more_than_2_digits_among_k_first_values(
        4, numbers) == 0


def test_get_sum_for_numbers_with_more_than_2_digits_among_k_first_values_use_case_2():
    numbers = [4, 5, 17, 9, 14, 108, -9, 12, 76]

    assert get_sum_for_numbers_with_more_than_2_digits_among_k_first_values(
        6, numbers) == 108


def test_get_sum_for_numbers_with_more_than_2_digits_among_k_first_values_use_case_3():
    numbers = [114, 215, -117, 119, 14, 108, -9, 12, 76]

    assert get_sum_for_numbers_with_more_than_2_digits_among_k_first_values(
        5, numbers) == 331


def test_get_sum_for_numbers_with_more_than_2_digits_among_k_first_values_use_case_4():
    numbers = [114, 215, -117, 119, 14, 108, -9, 12, 76]

    assert get_sum_for_numbers_with_more_than_2_digits_among_k_first_values(
        1, numbers) == 114


def test_get_sum_for_numbers_with_more_than_2_digits_among_k_first_values_with_k_negative():
    try:
        get_sum_for_numbers_with_more_than_2_digits_among_k_first_values(
            -1, [456, 723, 12, 47])
        fail('An exception should have been thrown by then')
    except ValueError as error:
        assert str(error) == 'k must be equal or greater than 1'

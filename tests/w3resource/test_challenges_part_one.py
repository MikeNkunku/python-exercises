from pytest import fail

from w3resource.challenges_part_one import *

__FAIL_ERROR_MESSAGE = 'An exception should have been thrown by then'


def internal_test_get_list_of_three_numbers_which_sum_to_target(
        numbers: list[int], expected_list_of_three_numbers: list[list[int]]):
    list_of_three_numbers = get_list_of_three_numbers_which_sum_to_target(
        numbers, 0)

    assert len(list_of_three_numbers) == len(expected_list_of_three_numbers)
    for expected_list in expected_list_of_three_numbers:
        assert expected_list in list_of_three_numbers


def test_get_list_of_three_numbers_which_sum_to_target_use_case_1():
    numbers = [-1, 0, 1, 2, -1, -4]
    expected_list_of_three_numbers = [[-1, -1, 2], [-1, 0, 1]]

    internal_test_get_list_of_three_numbers_which_sum_to_target(
        numbers, expected_list_of_three_numbers)


def test_get_list_of_three_numbers_which_sum_to_target_use_case_2():
    numbers = [1, 0, -1, 0, -2, 2]
    expected_list_of_three_numbers = [[-1, 0, 1], [-2, 0, 2]]

    internal_test_get_list_of_three_numbers_which_sum_to_target(
        numbers, expected_list_of_three_numbers)


def test_get_missing_numbers_from_list():
    numbers = [1, 2, 3, 4, 6, 7, 10]
    expected_missing_numbers = [5, 8, 9]

    missing_numbers = get_missing_numbers_from_list(numbers)

    assert len(missing_numbers) == len(expected_missing_numbers)
    for expected_missing_number in expected_missing_numbers:
        assert expected_missing_number in missing_numbers


def test_get_sum_digits_of_integer_until_single_digit_48_3():
    assert get_sum_digits_of_integer_until_single_digit(48) == 3


def test_get_sum_digits_of_integer_until_single_digit_59_5():
    assert get_sum_digits_of_integer_until_single_digit(59) == 5


def test_get_sum_digits_of_integer_until_single_digit_9_9():
    assert get_sum_digits_of_integer_until_single_digit(9) == 9


def test_get_sum_digits_of_integer_until_single_digit_with_negative_number():
    try:
        get_sum_digits_of_integer_until_single_digit(-8)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'Only positive numbers are accepted'


def test_is_arithmetic_sequence_less_than_two_numbers():
    try:
        is_arithmetic_sequence([1])
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'At least two numbers should be present'


def test_is_arithmetic_sequence_less_than_valid_use_case_false():
    numbers = [5, 7, 9, 11, 13, 14]
    assert not is_arithmetic_sequence(numbers)


def test_is_arithmetic_sequence_less_than_valid_use_case_true():
    numbers = [5, 7, 9, 11, 13, 15]
    assert is_arithmetic_sequence(numbers)


def test_is_integer_power_of_another_integer_use_case_1():
    assert is_integer_power_of_another_integer(16, 2)


def test_is_integer_power_of_another_integer_use_case_2():
    assert not is_integer_power_of_another_integer(3, 16)


def test_is_integer_power_of_another_integer_with_one_negative_number():
    try:
        is_integer_power_of_another_integer(-4, 64)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'Both numbers must be positive'


def test_is_string_anagram_of_another_string_use_case_1_true():
    assert is_string_anagram_of_another_string('anagram', 'nagaram')


def test_is_string_anagram_of_another_string_use_case_2_false():
    assert not is_string_anagram_of_another_string('okay', 'kaio')


def test_is_ugly_number_1_true():
    assert is_ugly_number(1)


def test_is_ugly_number_12_true():
    assert is_ugly_number(12)


def test_is_ugly_number_2_true():
    assert is_ugly_number(2)


def test_is_ugly_number_3_true():
    assert is_ugly_number(3)


def test_is_ugly_number_5_true():
    assert is_ugly_number(5)


def test_is_ugly_number_7_false():
    assert not is_ugly_number(7)

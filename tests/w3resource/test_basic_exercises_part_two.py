from pytest import fail

from w3resource.basic_exercises_part_two import *

__FAIL_ERROR_MESSAGE = 'An exception should have been thrown by then'


def test_are_all_numbers_different_false():
    assert not are_all_numbers_different([0, 1, 1, 2, 3, 5, 8])


def test_are_all_numbers_different_true():
    assert are_all_numbers_different([1, 2, 3, 5, 8])


def test_are_strings_isomorphic_use_case_1():
    assert not are_strings_isomorphic('foo', 'bar')


def test_are_strings_isomorphic_use_case_2():
    assert not are_strings_isomorphic('bar', 'foo')


def test_are_strings_isomorphic_use_case_3():
    assert are_strings_isomorphic('paper', 'title')


def test_are_strings_isomorphic_use_case_4():
    assert are_strings_isomorphic('title', 'paper')


def test_are_strings_isomorphic_use_case_5():
    assert not are_strings_isomorphic('apple', 'orange')


def test_are_strings_isomorphic_use_case_6():
    assert not are_strings_isomorphic('aa', 'ab')


def test_are_strings_isomorphic_use_case_7():
    assert not are_strings_isomorphic('ab', 'aa')


def test_get_10_random_even_numbers():
    random_even_numbers = get_10_random_even_numbers(1, 100)

    assert len(random_even_numbers) == 10
    for number in random_even_numbers:
        assert 1 <= number
        assert number <= 100
        assert number % 2 == 0


def test_get_all_possible_strings():
    all_strings = get_all_possible_strings(['a', 'e'])
    expectations = ['ae', 'ea']
    for expectation in expectations:
        assert expectation in all_strings


def test_get_common_divisors_13_21():
    assert not get_common_divisors(13, 21)


def test_get_common_divisors_15_110():
    assert get_common_divisors(15, 110) == [5]


def test_get_cumulative_sum_of_numbers_in_list_use_case_1():
    sample_input = [10, 20, 30, 40, 50, 60, 7]
    expectation = [10, 30, 60, 100, 150, 210, 217]
    assert get_cumulative_sum_of_numbers_in_list(sample_input) == expectation


def test_get_cumulative_sum_of_numbers_in_list_use_case_2():
    sample_input = [1, 2, 3, 4, 5]
    expectation = [1, 3, 6, 10, 15]
    assert get_cumulative_sum_of_numbers_in_list(sample_input) == expectation


def test_get_cumulative_sum_of_numbers_in_list_use_case_3():
    sample_input = [0, 1, 2, 3, 4, 5]
    expectation = [0, 1, 3, 6, 10, 15]
    assert get_cumulative_sum_of_numbers_in_list(sample_input) == expectation


def test_get_day_name_from_date_str():
    assert get_day_name_from_date_str('2012-10-17') == 'Wednesday'


def test_get_first_10_happy_numbers():
    assert get_first_n_happy_numbers(10) == [1, 7, 10, 13, 19, 23, 28, 31, 32, 44]


def test_get_largest_product_of_three_integers_with_less_than_three_integers():
    try:
        get_largest_product_of_three_integers([7, 2])
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'At least 3 numbers must be provided'


def test_get_first_missing_positive_integer_use_case_1():
    assert get_first_missing_positive_integer([2, 3, 7, 6, 8, -1, -10, 15,
                                               16]) == 4


def test_get_first_missing_positive_integer_use_case_2():
    assert get_first_missing_positive_integer([1, 2, 4, -7, 6, 8, 1, -10, 15])\
           == 3


def test_get_first_missing_positive_integer_use_case_3():
    assert get_first_missing_positive_integer([1, 2, 3, 4, 5, 6, 7]) == 8


def test_get_first_missing_positive_integer_use_case_4():
    assert get_first_missing_positive_integer([-2, -3, -1, 1, 2, 3]) == 4


def test_get_first_missing_positive_integer_use_case_5():
    assert not get_first_missing_positive_integer([-2, -3, -1, -4, -2, -3])


def test_get_largest_product_of_three_integers_use_case_1():
    assert get_largest_product_of_three_integers([-10, -20, 20, 1]) == 4000


def test_get_largest_product_of_three_integers_use_case_2():
    assert get_largest_product_of_three_integers([-1, -1, 4, 2, 1]) == 8


def test_get_largest_product_of_three_integers_use_case_3():
    assert get_largest_product_of_three_integers([1, 2, 3, 4, 5, 6]) == 120


def test_get_largest_profit_use_case_1():
    assert get_largest_profit([8, 10, 7, 5, 7, 15]) == 10


def test_get_largest_profit_use_case_2():
    assert get_largest_profit([1, 2, 8, 1]) == 7


def test_get_largest_profit_use_case_3():
    assert not get_largest_profit([])


def test_get_length_after_removing_duplicates_use_case_1():
    assert get_length_after_removing_duplicates([0, 0, 1, 1, 2, 2, 3, 3, 4, 4,
                                                 4]) == 5


def test_get_length_after_removing_duplicates_use_case_2():
    assert get_length_after_removing_duplicates([1, 2, 2, 3, 4, 4]) == 4


def test_get_list_sorted_in_descending_order():
    assert get_list_sorted_in_descending_order([45, 30, 10, 60, 22]) == [60, 45, 30, 22, 10]


def test_get_list_without_duplicated_numbers_use_case_1():
    assert get_list_without_duplicated_numbers([1, 2, 3, 2, 3, 4, 5])\
           == [1, 4, 5]


def test_get_list_without_duplicated_numbers_use_case_2():
    assert get_list_without_duplicated_numbers([1, 2, 3, 2, 4, 5])\
           == [1, 3, 4, 5]


def test_get_list_without_duplicated_numbers_use_case_3():
    assert get_list_without_duplicated_numbers([1, 2, 3, 4, 5])\
           == [1, 2, 3, 4, 5]


def test_get_missing_digits_from_phone_number_with_french_phone_number():
    result = get_missing_digits_from_phone_number('0712345678')
    assert len(result) == 1
    assert result[0] == 9


def test_get_missing_digits_from_phone_number_with_international_phone_number():
    result = get_missing_digits_from_phone_number('+33712345678')
    expectations = [0, 9]

    assert len(result) == len(expectations)
    for expectation in expectations:
        assert expectation in result


def test_get_most_frequent_word_and_longest_one_from_text():
    expectations = {'most_frequent_word': 'your', 'longest_word': 'participation'}

    result = get_most_frequent_word_and_longest_one_from_text(
        "Thank you for your comment and your participation."
    )

    assert len(result) == len(expectations)
    for key in expectations.keys():
        assert result[key] == expectations[key]


def test_get_number_of_arguments_when_a_list_is_provided():
    assert get_number_of_arguments([2, 3, 5, 8]) == 1


def test_get_number_of_arguments_when_four_args_provided():
    assert get_number_of_arguments(2, 3, 5, 8) == 4


def test_get_number_of_arguments_when_none_provided():
    assert not get_number_of_arguments()


def test_get_number_of_arguments_when_one_arg_provided():
    assert get_number_of_arguments(5) == 1


def test_get_number_of_combinations_for_goldbach_number_for_100():
    assert get_number_of_combinations_for_goldbach_number(100) == 6


def test_get_number_of_combinations_for_goldbach_number_for_11():
    try:
        get_number_of_combinations_for_goldbach_number(11)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'An even number must be provided'


def test_get_number_of_combinations_for_goldbach_number_for_2():
    try:
        get_number_of_combinations_for_goldbach_number(2)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'The provided number must be equal to or greater than 4'


def test_get_number_of_combinations_for_goldbach_number_for_6():
    assert get_number_of_combinations_for_goldbach_number(6) == 1


def test_get_number_of_permutations_to_reach_target():
    assert get_number_of_permutations_to_reach_target(15) == 336


def test_get_number_of_prime_numbers_less_than_provided_number_with_1():
    try:
        get_number_of_prime_numbers_less_than_provided_number(1)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        error_msg = 'The provided number should be equal or greater than 2'
        assert str(error) == error_msg


def test_get_number_of_prime_numbers_less_than_provided_number_with_10():
    assert get_number_of_prime_numbers_less_than_provided_number(10) == 4


def test_get_number_of_prime_numbers_less_than_provided_number_with_100():
    assert get_number_of_prime_numbers_less_than_provided_number(100) == 25


def test_get_oldest_person_name_use_case_1_nidia_dominique():
    persons = {"Bernita Ahner": 12, "Kristie Marsico": 11, "Sara Pardee":
        14, "Fallon Fabiano": 11, "Nidia Dominique": 15}
    assert get_oldest_person_name(persons) == 'Nidia Dominique'


def test_get_oldest_person_name_use_case_2_becki_saunder():
    persons = {"Nilda Woodside": 12, "Jackelyn Pineda": 12.2,
               "Sofia Park": 12.4, "Joannie Archibald": 12.6,
               "Becki Saunder": 12.7}
    assert get_oldest_person_name(persons) == 'Becki Saunder'


def test_get_original_string_from_compressed_text_use_case_1():
    assert get_original_string_from_compressed_text('XY#6Z1#4023') == 'XYZZZZZZ1000023'


def test_get_original_string_from_compressed_text_use_case_2():
    assert get_original_string_from_compressed_text('#39+1=1#30') == '999+1=1000'


def test_get_position_of_second_occurrence_in_string_use_case_1():
    assert get_position_of_second_occurrence_in_string("The quick brown fox "
                                                       "jumps over the lazy "
                                                       "dog", 'the') == -1


def test_get_position_of_second_occurrence_in_string_use_case_2():
    assert get_position_of_second_occurrence_in_string(
        "the quick brown fox jumps over the lazy dog", 'the') == 31


def test_get_text_without_duplicate_consecutive_letters_java_java():
    assert get_text_without_duplicate_consecutive_letters('Java') == \
           'Java'


def test_get_text_without_duplicate_consecutive_letters_ppphhhppp_php():
    assert get_text_without_duplicate_consecutive_letters('PPPHHHPPP') == \
           'PHP'


def test_get_text_without_duplicate_consecutive_letters_ppyyytthon_python():
    assert get_text_without_duplicate_consecutive_letters('PPYYYTTHON') == \
           'PYTHON'


def test_get_text_without_duplicate_consecutive_letters_ppyyythonnn_python():
    assert get_text_without_duplicate_consecutive_letters('PPyyythonnn') == \
           'Python'


def test_get_x_and_y_for_equation_raising_value_error():
    try:
        get_x_and_y_for_equation(1, 1, 2, 1, 1, 3)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == "'b*d - a*e' cannot be equal to 0"


def test_get_sum_of_25_first_prime_numbers():
    assert get_sum_of_n_first_prime_numbers(25) == 1060


def test_get_sum_of_values_multiplied_by_their_index_with_empty_list():
    assert not get_sum_of_values_multiplied_by_their_index([])


def test_get_sum_of_values_multiplied_by_their_index_with_negative_numbers():
    assert get_sum_of_values_multiplied_by_their_index([-1, -2, -3, -4]) == -20


def test_get_sum_of_values_multiplied_by_their_index_with_positive_numbers():
    assert get_sum_of_values_multiplied_by_their_index([1, 2, 3, 4]) == 20


def test_get_x_and_y_for_equation_with_valid_args():
    expectations = {'x': -2.00, 'y': 2.00}

    result = get_x_and_y_for_equation(5, 8, 6, 7, 9, 4)

    for key in result.keys():
        assert result[key] == expectations[key]


def test_is_13_a_happy_number_true():
    assert is_happy_number(13)


def test_is_4_a_happy_number_false():
    assert not is_happy_number(4)


def test_is_6_a_happy_number_false():
    assert not is_happy_number(6)


def test_is_7_a_happy_number_true():
    assert is_happy_number(7)


def test_is_932_a_happy_number_true():
    assert is_happy_number(932)


def test_is_employee_code_8_or_12_digits_use_case_1():
    assert is_employee_code_8_or_12_digits('12345678')


def test_is_employee_code_8_or_12_digits_use_case_2():
    assert not is_employee_code_8_or_12_digits('1234567j')


def test_is_employee_code_8_or_12_digits_use_case_3():
    assert not is_employee_code_8_or_12_digits('12345678j')


def test_is_employee_code_8_or_12_digits_use_case_4():
    assert is_employee_code_8_or_12_digits('123456789123')


def test_is_employee_code_8_or_12_digits_use_case_5():
    assert not is_employee_code_8_or_12_digits('123456abcdef')


def test_is_index_evenness_or_oddness_in_sync_with_value_use_case_1_true():
    assert is_index_evenness_or_oddness_in_sync_with_value([2, 1, 4, 3, 6, 7, 6, 3])


def test_is_index_evenness_or_oddness_in_sync_with_value_use_case_2_false():
    assert not is_index_evenness_or_oddness_in_sync_with_value([2, 1, 4, 3, 6, 7, 6, 4])


def test_is_index_evenness_or_oddness_in_sync_with_value_use_case_3_true():
    assert is_index_evenness_or_oddness_in_sync_with_value([4, 1, 2])


def test_has_sequence_an_increasing_trend_use_case_1_true():
    assert has_sequence_an_increasing_trend([1, 2, 3, 4])


def test_has_sequence_an_increasing_trend_use_case_2_false():
    assert not has_sequence_an_increasing_trend([1, 2, 5, 3, 4])


def test_has_sequence_an_increasing_trend_use_case_3_false():
    assert not has_sequence_an_increasing_trend([-1, -2, -3, -4])


def test_has_sequence_an_increasing_trend_use_case_4_true():
    assert has_sequence_an_increasing_trend([-4, -3, -2, -1])


def test_has_sequence_an_increasing_trend_use_case_5_false():
    assert not has_sequence_an_increasing_trend([1, 2, 3, 4, 0])


def test_is_115_132_219_018_763_992_565_095_597_973_971_522_401_narcissistic_true():
    assert is_narcissistic(115_132_219_018_763_992_565_095_597_973_971_522_401)


def test_is_153_narcissistic_true():
    assert is_narcissistic(153)


def test_is_1634_narcissistic_true():
    assert is_narcissistic(1634)


def test_is_370_narcissistic_true():
    assert is_narcissistic(370)


def test_is_407_narcissistic_true():
    assert is_narcissistic(407)


def test_is_409_narcissistic_false():
    assert not is_narcissistic(409)


def test_is_8208_narcissistic_true():
    assert is_narcissistic(8208)


def test_is_9474_narcissistic_true():
    assert is_narcissistic(9474)


def test_is_9475_narcissistic_false():
    assert not is_narcissistic(9475)


def test_is_palindrome_100_false():
    assert not is_palindrome(100)


def test_is_palindrome_252_true():
    assert is_palindrome(252)


def test_is_palindrome_minus_838_false():
    assert not is_palindrome(-838)

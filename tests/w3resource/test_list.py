from pytest import fail

from w3resource.list import *

__FAIL_ERROR_MESSAGE = 'An exception should have been thrown by then'


def test_get_all_sublists_of_list_with_0_element():
    try:
        get_all_sublists_of_list([])
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'Empty list'


def test_get_all_sublists_of_list_with_1_element():
    try:
        get_all_sublists_of_list([0])
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'No available sublist'


def internal_test_get_all_sublists_of_list(a_list: list,
                                           expected_sublists: list[list]):
    sublists = get_all_sublists_of_list(a_list)

    assert len(sublists) == len(expected_sublists)
    for sublist in expected_sublists:
        assert sublist in sublists


def test_get_all_sublists_of_list_with_3_elements():
    a_list = [0, 1, 2]
    expected_sublists = [[0], [1], [2], [0, 1], [0, 2], [1, 2]]
    internal_test_get_all_sublists_of_list(a_list, expected_sublists)


def test_get_all_sublists_of_list_with_4_elements():
    a_list = [0, 1, 2, 3]
    expected_sublists = [[0], [1], [2], [3], [0, 1], [0, 2], [0, 3], [1, 2],
                         [1, 3], [2, 3], [0, 1, 2], [0, 1, 3], [0, 2, 3],
                         [1, 2, 3]]
    internal_test_get_all_sublists_of_list(a_list, expected_sublists)


def internal_test_get_difference_between_lists(
        list_1: list, list_2: list, expected_list_diff: list):
    list_diff = get_difference_between_lists(list_1, list_2)

    assert len(list_diff) == len(expected_list_diff)
    for elt in expected_list_diff:
        assert elt in list_diff


def test_get_difference_between_lists_with_duplicates():
    list_1 = [0, 3, 1, 5, 2, 3, 5, 8]
    list_2 = [0, 3, 5, 7, 9]
    expected_list_diff = [1, 2, 3, 5, 8]

    internal_test_get_difference_between_lists(list_1, list_2,
                                               expected_list_diff)


def test_get_difference_between_lists_without_duplicates():
    list_1 = [0, 1, 2, 3, 5, 8]
    list_2 = [0, 3, 5, 7, 9]
    expected_list_diff = [1, 2, 8]

    internal_test_get_difference_between_lists(list_1, list_2,
                                               expected_list_diff)


def test_get_list_of_non_empty_tuples_sorted_in_increasing_order_using_last_element():
    initial_tuples = [(2, 5), (1, 2), (4, 4), (2, 3), (2, 1)]
    expected_sorted_tuples = [(2, 1), (1, 2), (2, 3), (4, 4), (2, 5)]
    sorted_tuples = \
        get_list_of_non_empty_tuples_sorted_in_increasing_order_using_last_element(
            initial_tuples)
    assert sorted_tuples == expected_sorted_tuples


def internal_test_get_intersection_of_lists(list_1: list, list_2: list,
                                            expected_intersection: list):
    intersection = get_intersection_of_lists(list_1, list_2)
    assert len(intersection) == len(expected_intersection)
    for elt in expected_intersection:
        assert elt in intersection


def test_get_intersection_of_lists_with_common_element():
    list_1 = [0, 3, 7, 9]
    list_2 = [1, 2, 3, 5, 7, 8]
    internal_test_get_intersection_of_lists(list_1, list_2, [3, 7])


def test_get_difference_between_consecutive_numbers_in_list_use_case_1():
    numbers = [1, 1, 3, 4, 4, 5, 6, 7]
    expected_consecutive_diff = [0, 2, 1, 0, 1, 1, 1]
    assert get_difference_between_consecutive_numbers_in_list(numbers)\
        == expected_consecutive_diff


def test_get_difference_between_consecutive_numbers_in_list_use_case_2():
    numbers = [4, 5, 8, 9, 6, 10]
    expected_consecutive_diff = [1, 3, 1, -3, 4]
    assert get_difference_between_consecutive_numbers_in_list(numbers)\
        == expected_consecutive_diff


def test_get_intersection_of_lists_with_common_element_and_duplicates():
    list_1 = [0, 3, 7, 3, 9]
    list_2 = [1, 2, 7, 3, 5, 7, 8]
    internal_test_get_intersection_of_lists(list_1, list_2, [3, 7])


def test_get_intersection_of_lists_with_no_common_element():
    list_1 = [0, 3, 7, 9]
    list_2 = [1, 2, 5, 8]
    internal_test_get_intersection_of_lists(list_1, list_2, [])


def test_get_list_with_zeroes_at_the_end():
    a_list = [3, 4, 0, 0, 0, 6, 2, 0, 6, 7, 6, 0, 0, 0, 9, 10, 7, 4, 4, 5, 3,
              0, 0, 2, 9, 7, 1]
    expected_list = [3, 4, 6, 2, 6, 7, 6, 9, 10, 7, 4, 4, 5, 3, 2, 9, 7, 1, 0,
                     0, 0, 0, 0, 0, 0, 0, 0]

    new_list = get_list_with_zeroes_at_the_end(a_list)

    assert new_list == expected_list


def test_get_number_of_strings_where_length_gte_2_and_same_first_and_last_chars():
    strings = ['abc', 'xyz', 'aba', '1221']
    assert get_number_of_strings_where_length_gte_2_and_same_first_and_last_chars(
        strings) == 2


def test_get_product_of_all_items_with_0_in_list():
    numbers = [0, 1, 3, 5, 7]
    assert get_product_of_all_items(numbers) == 0


def test_get_product_of_all_items_without_0_in_list():
    numbers = [1, 3, 5, 7]
    assert get_product_of_all_items(numbers) == 105


def test_get_sum_multiplied_by_length_after_rounding():
    numbers = [22.4, 4.0, -16.22, -9.1, 11.0, -12.22, 14.2, -5.2, 17.5]
    assert get_sum_multiplied_by_length_after_rounding(numbers) == 243


def test_have_lists_at_least_one_common_member_with_one_empty_list():
    assert not have_lists_at_least_one_common_member([], [2, 5, 4])


def test_have_lists_at_least_one_common_member_valid_lists_false():
    list_1 = [1, 3, 5, 7]
    list_2 = [0, 2, 4, 6]
    assert not have_lists_at_least_one_common_member(list_1, list_2)


def test_have_lists_at_least_one_common_member_valid_lists_true():
    list_1 = [1, 3, 5, 7, 0]
    list_2 = [0, 2, 4, 6, 10, 3]
    assert have_lists_at_least_one_common_member(list_1, list_2)

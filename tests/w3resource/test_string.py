from w3resource.string import *


def test_get_longest_word_and_length_from_list():
    words = ['important', 'apparently']
    expectations = {'word': 'apparently', 'length': 10}

    result = get_longest_word_and_length_from_list(words)

    for key in expectations.keys():
        assert result[key] == expectations[key]


def test_get_string_made_from_first_two_and_last_two_chars_with_w():
    assert get_string_made_from_first_two_and_last_two_chars('w') == ''


def test_get_string_made_from_first_two_and_last_two_chars_with_w3():
    assert get_string_made_from_first_two_and_last_two_chars('w3') == 'w3w3'


def test_get_string_made_from_first_two_and_last_two_chars_with_w3resource():
    assert get_string_made_from_first_two_and_last_two_chars('w3resource') == 'w3ce'


def test_get_suffixed_string_if_length_matches_with_ab():
    assert get_suffixed_string_if_length_matches('ab') == 'ab'


def test_get_suffixed_string_if_length_matches_with_surprising():
    assert get_suffixed_string_if_length_matches('surprising') == 'surprisingly'


def test_get_suffixed_string_if_length_matches_with_touch():
    assert get_suffixed_string_if_length_matches('touch') == 'touching'

from w3resource.pandas.data_series import *


def test_get_one_dimension_array_sample():
    data_series = get_one_dimension_array_sample()

    assert data_series.dtype == 'int64'
    assert data_series.size == 4
    assert all(data_series.values == [1, 3, 5, 8])

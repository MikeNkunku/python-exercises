from tempfile import TemporaryFile, TemporaryDirectory

from pytest import fail, mark

from w3resource.basic_exercises_part_one import *

__FAIL_ERROR_MESSAGE = 'Should have thrown an exception by then'


def test_convert_int_to_binary_with_leading_zeroes():
    assert convert_int_to_binary_with_leading_zeroes(12) == '00001100'


def test_file_exists_false():
    assert not file_exists('a_file.txt')


@mark.skip(reason='Failing on CI for now')
def test_file_exists_true():
    with TemporaryFile() as tmp_file:
        assert file_exists(tmp_file.name)
        tmp_file.close()


def test_get_builtin_info_abs():
    assert get_builtin_info('abs') == 'Return the absolute value of the argument.'


def test_get_builtin_info_dummy():
    try:
        get_builtin_info('dummy')
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'Not a builtin function'


def test_get_circle_area_with_radius_two():
    assert get_circle_area(2) == 4 * math.pi


def test_get_executing_file():
    assert get_executing_file() == 'basic_exercises_part_one.py'


def test_get_day_diff_with_format_not_respected():
    try:
        get_day_diff('18/02/2022', '15/02/2022')
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert "time data '18/02/2022' does not match format '%Y-%M-%d'" == str(error)


def test_get_day_diff_with_first_date_after_second_one():
    assert get_day_diff('2022-02-18', '2022-02-15') == 3


def test_get_day_diff_with_first_date_before_second_one():
    assert get_day_diff('2022-02-10', '2022-02-15') == 5


def test_get_divisible_numbers():
    assert get_divisible_numbers([35, 15, 23, 30, 135, 118], 15) == [15, 30, 135]


def test_get_even_numbers_from_list_with_empty_list():
    try:
        get_even_numbers_from_list([], 123456)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'At least one value should be provided'


def test_get_even_numbers_from_list_with_empty_list_as_result():
    numbers = [386, 462, 47, 418, 907, 344, 236, 375, 823, 566, 597, 978, 328, 615, 953, 345,
        399, 162, 758, 219, 918, 237, 412, 566, 826, 248, 866, 950, 626, 949, 687, 217,
        815, 67, 104, 58, 512, 24, 892, 894, 767, 553, 81, 379, 843, 831, 445, 742, 717,
        958, 743, 527]
    assert get_even_numbers_from_list(numbers, 247) == []


def test_get_even_numbers_from_list_with_valid_list():
    numbers = [386, 462, 47, 418, 907, 344, 236, 375, 823, 566, 597, 978, 328, 615, 953, 345,
        399, 162, 758, 219, 918, 237, 412, 566, 826, 248, 866, 950, 626, 949, 687, 217,
        815, 67, 104, 58, 512, 24, 892, 894, 767, 553, 81, 379, 843, 831, 445, 742, 717,
        958, 743, 527]
    expected_output = [386, 462, 418]
    assert get_even_numbers_from_list(numbers, 500) == expected_output


def test_get_file_extension_without_extension():
    try:
        get_file_extension('a_filename')
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'No extension to be found'


def test_get_file_extension_with_extension():
    assert get_file_extension('Dummy.java') == 'java'


def test_get_filenames_in_directory_nonexistent_path():
    try:
        get_filenames_in_directory('a_directory/')
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'The provided path does not exist'


@mark.skip(reason='Failing on CI for now')
def test_get_filenames_in_directory_with_file_path():
    tmp_file = TemporaryFile()
    try:
        get_filenames_in_directory(tmp_file.name)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'The provided path is not a directory'
    finally:
        tmp_file.close()


@mark.skip(reason='Failing on CI for now')
def test_get_filenames_in_directory_with_two_files_in_folder():
    tmp_dir = TemporaryDirectory()
    tmp_file_1 = TemporaryFile(dir=tmp_dir.name)
    tmp_file_2 = TemporaryFile(dir=tmp_dir.name)

    filenames = get_filenames_in_directory(tmp_dir.name)

    assert len(filenames) == 2
    assert os.path.basename(tmp_file_1.name) in filenames
    assert os.path.basename(tmp_file_2.name) in filenames


def test_get_future_amount():
    assert get_future_amount(10_000, 3.5, 7) == 12_722.79


def test_get_greatest_common_divisor_with_one_zero():
    assert get_greatest_common_divisor(45, 0) == 45


def test_get_greatest_common_divisor_with_two_zeroes():
    try:
        get_greatest_common_divisor(0, 0)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'Only one value can be zero'


def test_get_greatest_common_divisor_case_1():
    assert get_greatest_common_divisor(25, 20) == 5


def test_get_greatest_common_divisor_case_2():
    assert get_greatest_common_divisor(231, 63) == 21


def test_get_histogram_dict_with_valid_list():
    assert get_histogram_dict([1, 2, 3, 4, 1, 4, 5, 4, 5]) == {1: 2, 2: 1, 3: 1, 4: 3, 5: 2}


def test_get_histogram_dict_value_error():
    try:
        get_histogram_dict([])
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'At least one value should be present'


def test_get_list_diff_as_set_with_empty_second_list():
    first_list = ['Black', 'White', 'Yellow']
    assert get_list_diff_as_set(first_list, []) == set(first_list)


def test_get_list_diff_as_set_with_non_empty_second_list():
    first_list = ['Black', 'White', 'Yellow']
    assert get_list_diff_as_set(first_list, ['White']) == {'Black', 'Yellow'}


def test_get_product():
    assert get_product([2, 3, 5, 10]) == 300


def test_get_sorted_list_without_loops_nor_conditionals():
    assert get_sorted_list_without_loops_nor_conditionals(3, 1, 2) == [1, 2, 3]


def test_get_sum_digits_with_one_digit():
    assert get_sum_digits(9) == 9


def test_get_sum_digits_with_two_digits():
    assert get_sum_digits(29) == 11


def test_get_sum_of_all_cubes_for_ints_lower_than_provided_int():
    assert get_sum_of_all_cubes_for_ints_lower_than_provided_int(3) == 9


def test_is_odd_false():
    assert not is_odd(2)


def test_is_odd_true():
    assert is_odd(13)


def test_is_odd_value_error():
    try:
        is_odd(-1)
        fail(__FAIL_ERROR_MESSAGE)
    except ValueError as error:
        assert str(error) == 'A non-negative integer must be provided'

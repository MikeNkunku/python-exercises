#!/usr/bin/env python


def get_odd_numbers(number: int) -> list[int]:
	return [number for number in range(number + 1) if number % 2 == 1]


def is_element_in_list(element_to_look_for: str, elements: list) -> bool:
	return element_to_look_for in elements


def reverse_list(elements: list) -> list:
	return elements[::-1]


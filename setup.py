#!/usr/bin/env python


from distutils.core import setup


setup(
    name='Python Exercises',
    description='Project containing exercises for Python',
    author='Mike Nkunku',
    author_email='mikenkunku@gmail.com',
    python_requires='>=3.10'
)

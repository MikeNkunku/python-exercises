import datetime
import functools
import itertools
import re
from random import shuffle, sample


__EMPLOYEE_CODE_PATTERN = re.compile(r'\d{8}|\d{12}')


def are_all_numbers_different(numbers: list[int]) -> bool:
    a_dict = {}
    for num in numbers:
        if num not in a_dict.keys():
            a_dict[num] = 1
        else:
            a_dict[num] += 1

    return not [k for k, v in a_dict.items() if v > 1]


def are_strings_isomorphic(str1: str, str2: str) -> bool:
    length = len(str1)
    if length != len(str2):
        return False

    correspondences = {}
    for i in range(length):
        char1 = str1[i]
        char2 = str2[i]
        char1_absent = char1 not in correspondences.keys()
        char2_absent = char2 not in correspondences.values()
        keys_for_char2 = [k for k in correspondences.keys() if
                          correspondences[k] == char2]
        key_for_char2 = keys_for_char2[0] if len(keys_for_char2) != 0 else \
            None
        if char1_absent and char2_absent:
            correspondences[char1] = char2
        elif key_for_char2 != char1:
            return False

    replaced_str = ''.join(map(lambda c: correspondences[c], str1))
    return replaced_str == str2


def get_10_random_even_numbers(min_value_incl: int, max_value_incl: int) -> \
        list[int]:
    numbers = []

    while len([n for n in numbers if n % 2 == 0]) != 10:
        numbers = sample(range(min_value_incl, max_value_incl + 1), 10)

    return numbers


def get_all_possible_strings(letters: list[str]) -> list[str]:
    def factorial(number: int) -> int:
        return number if number == 1 else number * factorial(number - 1)

    result = []
    letters_copy = letters
    while len(result) < factorial(len(letters)):
        shuffle(letters_copy)
        string = ''.join(letters_copy)
        if string not in result:
            result.append(string)

    return result


def get_common_divisors(a: int, b: int) -> list[int]:
    return [n for n in range(2, min(a, b) + 1) if not a % n and not b % n]


def get_cumulative_sum_of_numbers_in_list(numbers: list[int]) -> list[int]:
    cumulative_sum = 0
    output = []
    for n in numbers:
        cumulative_sum += n
        output.append(cumulative_sum)

    return output


def get_day_name_from_date_str(date_str: str) -> str:
    date_format = '%Y-%m-%d'
    output_format = '%A'

    the_date = datetime.datetime.strptime(date_str, date_format)

    return the_date.strftime(output_format)


def get_first_n_happy_numbers(nb_happy_numbers: int) -> list[int]:
    number = 1
    nb = 0
    happy_numbers = []

    while nb < nb_happy_numbers:
        if is_happy_number(number):
            happy_numbers.append(number)
            nb += 1
        number += 1

    return happy_numbers


def get_largest_product_of_three_integers(numbers: list[int]) -> int:
    if len(numbers) < 3:
        raise ValueError('At least 3 numbers must be provided')

    return functools.reduce(lambda acc, comb: acc
                            if comb[0] * comb[1] * comb[2] < acc
                            else comb[0] * comb[1] * comb[2],
                            itertools.combinations(numbers, 3), 0)


def get_first_missing_positive_integer(numbers: list[int]) -> int:
    positive_numbers = [n for n in numbers if n > 0]
    nb_positive_numbers = len(positive_numbers)
    if nb_positive_numbers == 0:
        return 0

    for i in range(nb_positive_numbers - 1):
        next_positive_number = positive_numbers[i] + 1
        if positive_numbers[i + 1] != next_positive_number:
            return next_positive_number

    return positive_numbers[nb_positive_numbers - 1] + 1


def get_largest_profit(values: list[int]) -> int:
    return 0 if not values else max(values) - min(values)


def get_length_after_removing_duplicates(numbers: list[int]) -> int:
    return len(set(numbers))


def get_list_sorted_in_descending_order(numbers: list[int]) -> list[int]:
    numbers.sort()
    return numbers[::-1]


def get_list_without_duplicated_numbers(numbers: list[int]) -> list[int]:
    numbers_occurrences = {}
    for n in numbers:
        if n not in numbers_occurrences.keys():
            numbers_occurrences[n] = 1
        else:
            numbers_occurrences[n] += 1

    return [n for n in numbers_occurrences.keys()
            if numbers_occurrences[n] == 1]


def get_missing_digits_from_phone_number(phone_number: str) -> list[int]:
    result = [i for i in range(10)]
    for char in phone_number.replace('+', ''):
        digit = int(char)
        if digit in result:
            result.remove(digit)
    return result


def get_most_frequent_word_and_longest_one_from_text(text: str) -> dict:
    separator = '|'
    text = re.sub(r'\W', separator, text)
    words = text.split(separator)

    words_count = {}
    most_frequent_word = ''
    max_word_freq = 0
    longest_word = ''

    for word in words:
        if word not in words_count.keys():
            words_count[word] = 1
        else:
            words_count[word] += 1

        if len(longest_word) < len(word):
            longest_word = word

        word_count = words_count[word]
        if most_frequent_word != word and max_word_freq < word_count:
            max_word_freq = word_count
            most_frequent_word = word

    return {'most_frequent_word': most_frequent_word,
            'longest_word': longest_word}


def get_number_of_arguments(*args) -> int:
    return len(args)


def get_number_of_combinations_for_goldbach_number(number: int) -> int:
    if number < 4:
        raise ValueError(
            'The provided number must be equal to or greater than 4')

    if number % 2 != 0:
        raise ValueError('An even number must be provided')

    combinations = []
    for combination in itertools.combinations_with_replacement(range(number),
                                                               2):
        if not is_prime_number(combination[0]) or not is_prime_number(
                combination[1]):
            continue
        if sum(combination) == number:
            combinations.append(combination)

    return len(combinations)


def get_number_of_permutations_to_reach_target(number: int) -> int:
    matching_permutations = []
    for permutation in itertools.permutations(range(10), 4):
        if sum(permutation) == number:
            matching_permutations.append(permutation)

    return len(matching_permutations)


def get_number_of_prime_numbers_less_than_provided_number(number: int) -> int:
    if number < 2:
        raise ValueError(
            'The provided number should be equal or greater than 2')

    return len([n for n in range(number) if is_prime_number(n)])


def get_oldest_person_name(persons: dict[str, int | float]) -> str:
    return [name for name, age in persons.items()
            if age == max(persons.values())][0]


def get_original_string_from_compressed_text(compressed_text: str) -> str:
    text = ''
    i = 0
    while i < len(compressed_text):
        char = compressed_text[i]
        if char == '#':
            text += int(compressed_text[i + 1]) * compressed_text[i + 2]
            i += 3
        else:
            text += char
            i += 1

    return text


def get_position_of_second_occurrence_in_string(text: str, string: str) -> int:
    first_occurrence_idx = text.find(string)

    return -1 if first_occurrence_idx < 0\
        else text.find(string, first_occurrence_idx + 1)


def get_sum_of_n_first_prime_numbers(nb_prime_numbers: int) -> int:
    sum_prime_numbers = 0
    nb_added_prime_numbers = 0
    number = 2

    while nb_added_prime_numbers < nb_prime_numbers:
        if is_prime_number(number):
            sum_prime_numbers += number
            nb_added_prime_numbers += 1
        number += 1

    return sum_prime_numbers


def get_sum_of_values_multiplied_by_their_index(numbers: list[int]) -> int:
    return 0 if len(numbers) == 0\
        else functools.reduce(lambda acc, i: acc + i * numbers[i],
                              range(len(numbers)), 0)


def get_text_without_duplicate_consecutive_letters(text: str) -> str:
    output = ''
    text_length = len(text)
    i = 0
    while i < text_length:
        char = text[i]
        output += char
        i += 1
        while i < text_length and text[i] == char:
            i += 1
    return output


def get_x_and_y_for_equation(a: int, b: int, c: int, d: int, e: int,
                             f: int) -> dict:
    """
    Function to solve the following equations: "ax + by = c" & "dx + ey = f"

    :return: A dict containing x & y as keys
    """

    divider = b * d - a * e
    if divider == 0:
        raise ValueError("'b*d - a*e' cannot be equal to 0")

    return {'x': round((b * f - c * e) / divider, 2),
            'y': round((a * f - c * d) / -divider, 2)}


def has_sequence_an_increasing_trend(numbers: list[int] | range | set[int] |
                                     tuple | frozenset[int]) -> bool:
    for i in range(len(numbers) - 1):
        if numbers[i + 1] < numbers[i]:
            return False

    return True


def is_employee_code_8_or_12_digits(code: str) -> bool:
    return __EMPLOYEE_CODE_PATTERN.fullmatch(code) is not None


def is_happy_number(n: int) -> bool:
    def helper(number: int, digit_sum: int,
               processed_values: list[int]) -> bool:
        if number == 1 or digit_sum == 1:
            return True

        if digit_sum in processed_values:
            return False

        processed_values.append(number)
        processed_values.append(digit_sum)

        return helper(digit_sum, functools.reduce(
            lambda acc, digit: acc + int(digit) ** 2, str(digit_sum), 0),
                      processed_values)

    return helper(n, functools.reduce(lambda acc, digit: acc + int(digit) ** 2,
                                      str(n), 0), [])


def is_index_evenness_or_oddness_in_sync_with_value(numbers: list[int]) -> \
        bool:
    for i in range(len(numbers)):
        if i % 2 and not numbers[i] % 2 or (not i % 2 and numbers[i] % 2):
            return False

    return True


def is_narcissistic(number: int) -> bool:
    number_str = str(number)
    power = len(number_str)

    return functools.reduce(lambda acc, digit: acc + int(digit) ** power,
                            number_str, 0) == number


def is_palindrome(n: int) -> bool:
    str_n = str(n)
    return False if n < 0 else str_n == str_n[::-1]


def is_prime_number(n: int) -> bool:
    if n == 0 or n == 1:
        return False

    for divider in range(2, n):
        if not n % divider:
            return False

    return True

import functools
import itertools


def get_list_of_three_numbers_which_sum_to_target(
        numbers: list[int], target_sum: int) -> list[list[int]]:
    output = []
    for combination in itertools.combinations(numbers, 3):
        three_numbers = list(combination)
        three_numbers.sort()
        if sum(three_numbers) == target_sum and three_numbers not in output:
            output.append(three_numbers)
    return output


def get_missing_numbers_from_list(numbers: list[int]) -> list[int]:
    return [n for n in range(1, max(numbers)) if n not in numbers]


def get_sum_digits_of_integer_until_single_digit(number: int) -> int:
    """
     Adds the digits of a positive integer repeatedly until the result has a
     single digit.

    :param number: A positive number.
    :return: A single digit corresponding to the last sum of digit.
    """
    if number < 0:
        raise ValueError('Only positive numbers are accepted')

    digit_sum = number
    while digit_sum >= 10:
        digit_sum = functools.reduce(lambda acc, digit_str: acc + int(
            digit_str), str(digit_sum), 0)

    return digit_sum


def is_arithmetic_sequence(numbers: list[float]) -> bool:
    """
    In mathematics, an arithmetic progression or arithmetic sequence is a
    sequence of numbers such that the difference between the consecutive terms
    is constant.

    :param numbers: The list of numbers to consider.
    :return: Whether the provided list is an arithmetic sequence.
    """

    if len(numbers) < 2:
        raise ValueError('At least two numbers should be present')

    common_diff = numbers[1] - numbers[0]
    for i in range(2, len(numbers)):
        if numbers[i] - numbers[i - 1] != common_diff:
            return False

    return True


def is_integer_power_of_another_integer(number_1: int, number_2: int) -> bool:
    if number_1 < 0 or number_2 < 0:
        raise ValueError('Both numbers must be positive')

    min_val = min(number_1, number_2)
    max_val = max(number_1, number_2)

    return len(list(filter(lambda power: min_val ** power == max_val,
                           range(max_val)))) == 1


def is_string_anagram_of_another_string(string_1: str, string_2: str) -> bool:
    if len(string_1) != len(string_2):
        return False

    dict_1 = {}
    dict_2 = {}
    for i in range(len(string_1)):
        char_1 = string_1[i]
        if char_1 not in dict_1.keys():
            dict_1[char_1] = 1
        else:
            dict_1[char_1] += 1

        char_2 = string_2[i]
        if char_2 not in dict_2.keys():
            dict_2[char_2] = 1
        else:
            dict_2[char_2] += 1

    if len(dict_1.keys()) != len(dict_2.keys()):
        return False

    for key in dict_1.keys():
        if key not in dict_2.keys() or dict_1[key] != dict_2[key]:
            return False

    return True


def is_ugly_number(number: int) -> bool:
    """
    Ugly numbers are positive numbers whose only prime factors are 2, 3 or 5.
    The sequence 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, ... shows the first 10 ugly
    numbers.

    :param number: The number to check.
    :return: Whether the provided number is ugly.
    """

    if number in [1, 2, 3, 5]:
        return True

    is_even = not number % 2
    is_divisible_by_3 = not number % 3
    is_divisible_by_5 = not number % 5
    if not (is_even or is_divisible_by_3 or is_divisible_by_5):
        return False

    for n in range(2, number):
        is_divisible_by_n = not number % n
        is_n_divisible_by_accepted_dividers = not n % 2 or not n % 3 or\
            not n % 5
        if is_divisible_by_n and not is_n_divisible_by_accepted_dividers:
            return False

    return True

# https://w3resource.com/python-exercises/pandas/index-data-series.php


import pandas as pd


def get_one_dimension_array_sample() -> pd.Series:
    return pd.Series([1, 3, 5, 8])

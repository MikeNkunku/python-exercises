from functools import reduce


def get_longest_word_and_length_from_list(words: list[str]) -> dict:
    output = {'word': reduce(lambda lw, word: word if len(word) > len(lw) else lw, words, '')}
    output['length'] = len(output['word'])

    return output


def get_string_made_from_first_two_and_last_two_chars(text: str) -> str:
    return '' if len(text) < 2 else text[:2] + text[-2:]


def get_suffixed_string_if_length_matches(word: str) -> str:
    word_length = len(word)
    if word_length < 3:
        return word

    last_3_letters = word[word_length-3:]

    return word + 'ly' if last_3_letters == 'ing' else word + 'ing'

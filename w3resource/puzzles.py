# https://www.w3resource.com/python-exercises/puzzles/index.php
import functools
import itertools
import sys


def are_exactly_four_distinct_values_present_without_consecutive_duplicates_for_first_twenty_entries(numbers: list[int])\
        -> bool:
    numbers_occurrences = {}
    i = 0
    while i < len(numbers) - 1:
        number = numbers[i]
        if number not in numbers_occurrences:
            numbers_occurrences[number] = 1
        else:
            numbers_occurrences[number] = +1
        if numbers[i + 1] == number:
            return False
        i += 1

    return len(numbers_occurrences.keys()) == 4


def get_indexes_of_numbers_below_provided_threshold(threshold: int,
                                                    numbers: list[int]) -> \
        list[int]:
    return [i for i in range(len(numbers)) if numbers[i] <= threshold]


def get_indexes_of_numbers_which_sum_to_zero(
        numbers: list[int] | tuple | range) -> tuple[int, int] | None:
    for idx_1, idx_2 in itertools.combinations(range(len(numbers)), 2):
        if numbers[idx_1] + numbers[idx_2] == 0:
            return idx_1, idx_2

    return None


def get_indices_for_which_numbers_in_list_drop(numbers: list[int])\
        -> list[int]:
    return [i for i in range(1, len(numbers)) if numbers[i] < numbers[i - 1]]


def get_list_of_strings_with_fewer_total_characters(lists: list[list[str]]) \
        -> list[str]:
    if len(lists) == 0:
        raise ValueError('At least one list should be present')

    min_total_length = sys.maxsize * 2 + 1
    list_of_strings_idx = 0
    for i in range(len(lists)):
        total_length = functools.reduce(lambda length, word: length
                                        + len(word), lists[i], 0)
        if min_total_length < total_length:
            min_total_length = total_length
            list_of_strings_idx = i

    return lists[list_of_strings_idx]


def get_list_sorted_by_digits_sum(numbers: list[int]) -> list[int]:
    def get_digit_sum(number: int) -> int:
        return functools.reduce(
            lambda digit_sum, digit_str: digit_sum + int(digit_str),
            str(number), 0)

    numbers.sort(key=get_digit_sum)
    return numbers


def get_positions_of_uppercase_vowels_at_even_indices_in_string(
        string: str) -> tuple:
    matching_letters = 'AEIOU'
    return tuple(filter(lambda i: i % 2 == 0 and string[i] in
                        matching_letters, range(len(string))))


def get_product_of_odd_digits_in_number(number: int) -> int:
    filtered_digits = tuple(map(lambda digit_str: int(digit_str),
                            filter(lambda digit: int(digit) % 2 == 1,
                                   str(number))))
    return 0 if len(filtered_digits) == 0 else functools.reduce(
        lambda product, digit: product * digit, filtered_digits, 1)


def get_string_split_by_spaces_or_commas(string: str) -> list[str]:
    """
    Splits a given string (s) into strings if there is a space in the string,
    otherwise split on commas if there is a comma, otherwise return the list
    of lowercase letters with odd order (order of a = 0, b = 1, etc.).

    :param string: The string which may contain spaces, commas or none of the
    mentioned ones.
    :return: A list of letters
    """

    if string.find(' ') != -1:
        return string.split(' ')

    if string.find(',') != -1:
        return string.split(',')

    return [string[i] for i in range(len(string)) if i % 2 == 1]


def get_strings_containing_substring(substring: str,
                                     strings: list[str] | tuple) -> \
        list[str]:
    return list(filter(lambda string: string.find(substring) != -1, strings))


def get_strings_lengths(strings: list[str]) -> list[int]:
    return list(map(lambda string: len(string), strings))


def get_sum_for_numbers_with_more_than_2_digits_among_k_first_values(
        k: int, numbers: list[int] | tuple | range) -> int:
    if k < 1:
        raise ValueError('k must be equal or greater than 1')

    return functools.reduce(lambda res, i: res if len(str(numbers[i])) <= 2
                            else res + numbers[i], range(k), 0)


def get_sum_of_ascii_values_for_uppercase_letters_in_string(string: str) -> \
        int:
    return functools.reduce(lambda acc, letter: acc + ord(letter) if
                            letter.isupper() else acc, string, 0)

#!/usr/bin/env python
import builtins
import math
import os.path
import types
from datetime import datetime
from functools import reduce
from pathlib import Path


def convert_int_to_binary_with_leading_zeroes(number: int) ->  str:
    return f'{number:08b}'


def file_exists(file_path: str) -> bool:
    return Path(file_path).exists()


def get_builtin_info(builtin_name: str) -> str:
    builtins_obj = None
    for name, obj in vars(builtins).items():
        if isinstance(obj, types.BuiltinFunctionType) and name == builtin_name:
            builtins_obj = obj
            break

    if builtins_obj is None:
        raise ValueError('Not a builtin function')

    return builtins_obj.__doc__


def get_circle_area(radius: float) -> float:
    return 2 * math.pi * radius


def get_current_username() -> str:
    return os.getlogin()


def get_day_diff(first_date: str, second_date: str) -> int:
    date_format = '%Y-%M-%d'
    date_one = datetime.strptime(first_date, date_format)
    date_two = datetime.strptime(second_date, date_format)

    return abs((date_one - date_two).days)


def get_divisible_numbers(numbers: list[int], divider: int) -> list[int]:
    return list(filter(lambda n: n % divider == 0, numbers))


def get_even_numbers_from_list(numbers: list[int], limit: int) -> list[int]:
    if len(numbers) == 0:
        raise ValueError('At least one value should be provided')

    output = []
    for number in numbers:
        if number >= limit:
            break
        if number % 2 == 0:
            output.append(number)

    return output


def get_executing_file() -> str:
    return os.path.basename(__file__)


def get_file_extension(filename: str) -> str:
    if filename.find('.') == -1:
        raise ValueError('No extension to be found')

    return filename.split('.')[1]


def get_filenames_in_directory(directory_path: str) -> list[str]:
    folder_path = Path(directory_path)
    if not folder_path.exists():
        raise ValueError('The provided path does not exist')

    if not folder_path.is_dir():
        raise ValueError('The provided path is not a directory')

    file_names = []
    for dir_path, dir_names, filenames in os.walk(folder_path):
        file_names.extend(filenames)

    return file_names


def get_future_amount(initial_amount: float, interest_rate: float, nb_years: int) -> float:
    result = initial_amount
    i = 0
    while i < nb_years:
        result += result * interest_rate / 100
        i += 1

    return round(result, 2)


def get_greatest_common_divisor(a: int, b: int) -> int:
    if a == 0 and b == 0:
        raise ValueError('Only one value can be zero')

    if a == 0:
        return b
    if b == 0:
        return a

    result = min(a, b)
    while result > 1:
        if not a % result and not b % result:
            return result
        result -= 1

    return result


def get_histogram_dict(numbers: list[int]) -> dict:
    if len(numbers) == 0:
        raise ValueError('At least one value should be present')

    output = {}
    for i in numbers:
        if i not in output.keys():
            output[i] = 1
        else:
            output[i] += 1

    return output


def get_list_diff_as_set(first_list: list, second_list: list) -> set:
    if not len(second_list):
        return set(first_list)

    return set([elt for elt in first_list if elt not in second_list])


def get_product(numbers: list[int]) -> int:
    return reduce(lambda a, b: a * b, numbers)


def get_sorted_list_without_loops_nor_conditionals(a: int, b: int, c: int) -> list[int]:
    output = [a, b, c]
    output.sort()
    return output


def get_sum_digits(number: int) -> int:
    number_str = str(number)
    nb_digits = len(number_str)
    if nb_digits == 1:
        return number

    result = 0
    for i in range(nb_digits):
        result += int(number_str[i])

    return result


def get_sum_of_all_cubes_for_ints_lower_than_provided_int(number: int) -> int:
    return reduce(lambda acc, num: acc + num ** 3, range(number), 0)


def is_odd(number: int) -> bool:
    if number < 0:
        raise ValueError('A non-negative integer must be provided')

    return number % 2 == 1

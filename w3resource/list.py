# Exercises can be found at https://www.w3resource.com/python-exercises/list/
import functools


def get_all_sublists_of_list(a_list: list) -> list[list]:
    nb_elts = len(a_list)

    if nb_elts == 0:
        raise ValueError('Empty list')

    if nb_elts == 1:
        raise ValueError('No available sublist')

    res = [[elt] for elt in a_list]
    def add_sublist(target_size: int, elt_to_add_idx: int,
                    data_to_add: list) -> None:
        data_to_add.append(a_list[elt_to_add_idx])
        if len(data_to_add) == target_size:
            if data_to_add not in res:
                res.append(data_to_add)
            return

        for idx in range(elt_to_add_idx + 1, nb_elts):
            add_sublist(target_size, idx, data_to_add.copy())

    for size in range(2, nb_elts):
        for start_idx in range(nb_elts - size + 1):
            add_sublist(size, start_idx, [])

    return res


def get_difference_between_consecutive_numbers_in_list(numbers: list[int])\
        -> list[int]:
    res = []

    for i in range(len(numbers) - 1):
        res.append(numbers[i + 1] - numbers[i])

    return res


def get_difference_between_lists(list_1: list, list_2: list) -> list:
    res = list_1.copy()
    for elt in list_2:
        if elt in res:
            res.remove(elt)

    return res


def get_intersection_of_lists(list_1: list, list_2: list) -> list:
    res = []
    for elt in list_1:
        if elt in list_2:
            res.append(elt)
            list_2.remove(elt)

    return res


def get_list_of_non_empty_tuples_sorted_in_increasing_order_using_last_element(
        tuples: list[tuple]) -> list[tuple]:
    tuples.sort(key=lambda t: t[-1])
    return tuples


def get_list_with_zeroes_at_the_end(a_list: list[int]) -> list[int]:
    nb_zeroes = 0
    new_list = []
    for number in a_list:
        if number == 0:
            new_list.append(0)
            nb_zeroes += 1
        else:
            new_list.insert(len(new_list) - nb_zeroes, number)

    return new_list


def get_number_of_strings_where_length_gte_2_and_same_first_and_last_chars(
        strings: tuple | list[str]) -> int:
    return len(tuple(filter(lambda word: len(word) >= 2
                            and word[0] == word[-1], strings)))


def get_product_of_all_items(numbers: list[int]) -> int:
    return functools.reduce(lambda product, n: product * n, numbers, 1)


def get_sum_multiplied_by_length_after_rounding(numbers: list[float]) -> int:
    return sum(map(lambda n: round(n), numbers)) * len(numbers)


def have_lists_at_least_one_common_member(list_1: list, list_2: list) -> bool:
    if not list_1 or not list_2:
        return False

    for elt in list_1:
        if elt in list_2:
            return True

    return False
